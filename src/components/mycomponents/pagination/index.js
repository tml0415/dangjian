import paginationA from './pagination.vue'
const pagination={
    install:function (Vue) {
        Vue.component('pagination',paginationA)
    }
}
export default pagination