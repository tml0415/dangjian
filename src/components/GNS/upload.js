var myWorker={};
myWorker.options={
	maxSize:10 * 1024 * 1024 * 1024, // 上传最大文件限制
	eachSize: 1 * 1024 * 1024, // 每块文件大小
	data:{},
	// upload控件支持的参数 必须项
	action:'',
	files:[],
	headers:'',
	// upload控件支持的参数 非必须项
	withCredentials:false,
	name:'file',
	// 回调事件
	onProgress:'',
	onSuccess: '',
    onError: '',
};
myWorker.set_options=function(options){
    myWorker.options.files=options.file;
    myWorker.options.action=options.action;
    myWorker.options.data=options.data;
    myWorker.options.onProgress=options.onProgress;
    myWorker.options.onSuccess=options.onSuccess;
    myWorker.options.onError=options.onError;
    myWorker.Start();
}
myWorker.Start=async function(){
	let filenames=[];
	let checkedFile_msg=myWorker.checkedFile();
	if(checkedFile_msg!=''){
        if(typeof myWorker.options.onError==="function"){
            myWorker.options.onError(checkedFile_msg);
        }
		return;
	}
	for (let index = 0; index < myWorker.options.files.length; index++) {
		const fileOne = myWorker.options.files[index];
		let fileformDatas = await myWorker.splitFile(fileOne);
		if(fileformDatas=='文件切块发生错误'){
			if(typeof myWorker.options.onError==="function"){
				myWorker.options.onError(checkedFile_msg);
			}
			return;
		}
		let data = await myWorker.splitUpload(fileformDatas,index,myWorker.options.files.length);
		if(data==false){
			if(typeof myWorker.options.onError==="function"){
				myWorker.options.onError('上传文件失败');
			}
			return;
		}
		else{
			filenames.push(data.filename);
		}
	}
	if(typeof myWorker.options.onSuccess==="function"){
		myWorker.options.onSuccess(filenames);
	}
}
myWorker.checkedFile= function(){
	for (let index = 0; index < myWorker.options.files.length; index++) {
		const fileOne = myWorker.options.files[index];
		if (fileOne.size > myWorker.options.maxSize) {
			return '您选择的文件中有大于规定最大数值10G的！';
		}
	}
	return '';
}
myWorker.splitUpload= function(fileformDatas,index,length){
	return new Promise(async (resolve, reject) => {
		try {
            let currentChunk = 0;let needConvert=false;let filename='';
			for (let i = 0; i < fileformDatas.length; i++) {
				let Se=await myWorker.postFile(fileformDatas[i]);
				if(Se==false){
					reject(false);break;
				}
				else{
					if(i==fileformDatas.length-1){
                        if(typeof myWorker.options.onProgress==="function"){
                            myWorker.options.onProgress(100,index+1,length);
                        }
                        needConvert=Se.RequestEnd;
                        filename=Se.Message;
					}
					else{
                        if(typeof myWorker.options.onProgress==="function"){
                            myWorker.options.onProgress(Number(((i+1) / fileformDatas.length * 100).toFixed(0)),index,length);
                        }
					}
				}
			  }
			  resolve({status:true,needConvert:needConvert,filename:filename})
		} catch (e) {
			  reject(e)
		}
	})

}
myWorker.splitFile= function(file){
	return new Promise((resolve, reject) => {
		try {
			  setTimeout(() => {
				//对文件按照eachSizei进行分片
				let chunks = Math.ceil(file.size / myWorker.options.eachSize)
				let fileChunk = []
				for (let chunk = 0; chunks > 0; chunks--) {
					  fileChunk.push(file.slice(chunk, chunk + myWorker.options.eachSize))
					  chunk += myWorker.options.eachSize
				}
				const fileformDatas = []
				for (let i = 0; i < fileChunk.length; i++) {
					let postformdata={};
					postformdata[myWorker.options.name]=fileChunk[i];
					for(var key in myWorker.options.data){ //额外添加的数据 从data传入
						postformdata[key]=myWorker.options.data[key];   
					}
					let split={};
					//分块上传后台结构化文件必须添加的数据信息
					split["chunk"]=i;
					split["chunks"]=fileChunk.length;
					split["eachSize"]=myWorker.options.eachSize;
					split["fileName"]=file.name;
					split["fullSize"]=file.size;
					split["uid"]=file.uid;
					postformdata["split"]=JSON.stringify(split);
					let formData = new FormData()
					for (let p in postformdata) {
						formData.append(p, postformdata[p])
					}
					fileformDatas.push(formData);
				}
				resolve(fileformDatas)//对文件按照eachSizei进行分片
			  }, 0)
		} catch (e) {
			  reject('文件切块发生错误')
		}
	})
}
myWorker.postFile= function(formData){
	return new Promise((resolve, reject) => {
		myWorker.Ajax("POST",myWorker.options.action,formData)
		.then(function(data){
			if(data.Status==true){
				resolve(data)
			}
			else{
				resolve(false)
			}
		},function(data){
			resolve(false)
		});
	})
}
myWorker.Ajax=function(type,url,data,async=true){

	return new Promise((resolve, reject) => {
		var xmlHttp = new XMLHttpRequest(); //1.创建ajax对象
		xmlHttp.withCredentials=true;
		xmlHttp.onreadystatechange = function(){ //2.绑定监听函数
			//判断数据是否正常返回
			if(xmlHttp.readyState==4&&xmlHttp.status==200){
				//6.接收数据
				var res = xmlHttp.responseText;
				try{
					resolve(JSON.parse(res))
				}	
				catch(error)
				{
					reject(false)
				}
			}
		}
		xmlHttp.open(type,url,async); //3.绑定处理请求的地址,true为异步，false为同步
		//xmlHttp.setRequestHeader("Content-type","multipart/form-data"); //4.POST提交设置的协议头（GET方式省略）
		xmlHttp.send(data); ////5.发送请求 POST提交将参数，如果是GET提交send不用提交参数
	});
}
export default myWorker