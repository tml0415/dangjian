import Vue from 'vue'
import App from './App.vue'
Vue.config.productionTip = false
import ElementUI from 'element-ui';Vue.use(ElementUI); //使用ele
import 'element-ui/lib/theme-chalk/index.css';
import iView from 'iview';Vue.use(iView);
import 'iview/dist/styles/iview.css';
import VCharts from 'v-charts';Vue.use(VCharts)

import VueProgressiveImage from 'vue-progressive-image';Vue.use(VueProgressiveImage)
import Vuex from 'vuex';Vue.use(Vuex)
import './directives.js' //diolog拖动
import './css/index.css'/*引入公共样式*/
import './css/font1.css'/*引入公共样式*/
import Common from "./js/Common.js";
Vue.prototype.$Common=Common;//添加入vue原型链
import upload from "./js/upload.js";
Vue.prototype.$upload=upload;//添加入vue原型链
//注册功能组件
// import myLoading from "./components/myLoading"//自定义loading组件
// Vue.use(myLoading)   mycomponents
import Loginagain from "./components/mycomponents/Loginagain";Vue.use(Loginagain) //重新Login
import BreadcrumbA from "./components/mycomponents/BreadcrumbA";Vue.use(BreadcrumbA) //面包屑组件
import lcjl from "./components/mycomponents/lcjl";Vue.use(lcjl)  //查询历次纪录组件
import showfiles from "./components/mycomponents/showfiles";Vue.use(showfiles) //显示文件得组件
import showfilescommon from "./components/mycomponents/showfilescommon";Vue.use(showfilescommon) //显示文件得组件2
import pagination from "./components/mycomponents/pagination";Vue.use(pagination) //分页组件自定义
import Filerecordes from "./components/mycomponents/Filerecordes";Vue.use(Filerecordes) //换届选举 民主生活会 专题组织生活会 中心组学习 三会一课的统一模板  没有三级选择的
import FilerecordesThree from "./components/mycomponents/FilerecordesThree";Vue.use(FilerecordesThree) //有三级选择的
//组织建设中的特殊功能
import persons from "./components/mycomponents/zzjs/persons";Vue.use(persons) //组织建设 人员管理
import banzijianshe from "./components/mycomponents/zzjs/banzijianshe";Vue.use(banzijianshe) //组织建设 班子建设
import fldj from "./components/mycomponents/zzjs/fldj";Vue.use(fldj) //组织建设 分类定级
import bayrj from "./components/mycomponents/zzjs/bayrj";Vue.use(bayrj) //组织建设 博爱一日捐
import dfei from "./components/mycomponents/zzjs/dfei";Vue.use(dfei) //组织建设 党费管理
import dyfz from "./components/mycomponents/zzjs/dyfz";Vue.use(dyfz) //组织建设 党员发展
import dybf from "./components/mycomponents/zzjs/dybf";Vue.use(dybf) //组织建设 党员帮扶
import xsdx from "./components/mycomponents/zzjs/xsdx";Vue.use(xsdx) //组织建设 选树典型
import dypx from "./components/mycomponents/zzjs/dypx";Vue.use(dypx) //思想建设 党员培训
import jlcf from "./components/mycomponents/zzjs/jlcf";Vue.use(jlcf) //纪律建设  纪律处分

// import sjtj from './components/sjtj';Vue.use(sjtj) //数据统计
//数据统计
import sjtj_person from './components/sjtj/sjtj_person';Vue.use(sjtj_person) //数据统计
import sjtj_dangfei from './components/sjtj/sjtj_dangfei';Vue.use(sjtj_dangfei) //数据统计
// import sjtj_jjfz from './components/sjtj/sjtj_jjfz';Vue.use(sjtj_jjfz) //数据统计
// import sjtj_ybdy from './components/sjtj/sjtj_ybdy';Vue.use(sjtj_ybdy) //数据统计
// import sjtj_dybf from './components/sjtj/sjtj_dybf';Vue.use(sjtj_dybf) //数据统计
import sjtj_bayrj from './components/sjtj/sjtj_bayrj';Vue.use(sjtj_bayrj) //数据统计


import dyjf from './components/mycomponents/dyjf';Vue.use(dyjf) //党员积分
import router from './js/router.js' //路由
import store from './js/store'
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
