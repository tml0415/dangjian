import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import login from '@/components/login.vue'
import view from '@/components/view.vue'
import dw from '@/components/dw.vue'
import classify from '@/components/classify.vue'
//5大建设
import Menu from "@/components/routers/Menu.vue";
import mainwork from '@/components/routers/mainwork.vue' //具体工作的路由
import wjgl from '@/components/GNS/wjgl/wjgl.vue' //收发文管理
import manager from '@/components/manager/manager.vue' //后台管理
Vue.use(Router)
/*配置路由*/
const router = new Router({
  mode: 'history',
  base: 'testdj',
  routes: [
    //login
    {
      path: '/',
      name: 'login',
      component: login,
    },
    //view
    {
      path: '/view',
      name: 'view',
      component: view,
      beforeEnter: (to, from, next) => {
        store.dispatch('WriteActiveDwId_DzbId', {
          dwid: -1,
          dzbid: -1
        })
        next();
      }
    },
    //dw
    {
      path: '/dw',
      name: 'dw',
      component: dw,
      beforeEnter: (to, from, next) => {
        if (to.params.dwid && to.params.dzbid) {
          store.dispatch('WriteActiveDwId_DzbId', {
            dwid: to.params.dwid,
            dzbid: to.params.dzbid
          })
        }
        next();
      }
    },
    {
      path: '/classify',
      name: 'classify',
      component: classify
    },
    {
      path: '/Menu',
      name: 'Menu',
      component: Menu,
    },
    {
      path: '/mainwork',
      name: 'mainwork',
      component: mainwork,
    },
    {
      path: '/manager',
      name: 'manager',
      component: manager
    },
    {
      path: '/wjgl',
      name: 'wjgl',
      component: wjgl
    },
  ]
});
export default router;