import Vue from 'vue'
import Vuex from 'vuex'
import Common from "../js/Common.js";
Vue.use(Vuex)
const store = new Vuex.Store({
  state:{
    user:{},
    images:{
      danghui:require('../images/danghui.png'),
      danghui1:require('../images/danghui1.png'),
      dwdzbbj:require('../images/dwdzbbj.jpg'),
      dangqi:require('../images/dangqi.png'),
      hongqi:require('../images/hongqi.png'), //dangqi.png
      wujiaoxing:require('../images/wujiaoxing.png'),
    },
    Dw_DzbID:{},
    dwdzbs:[],
    breadcrumb:[],
    breadcrumbAAAA:{
      view:{
        name:'view',
        value:'测绘地理信息局委员会',
        show:false,
        params:{
          dwid:-1,
          dzbid:-1
        }
      },
      dw:{
        name:'dw',
        value:'??',
        show:false,
        params:{dwid:-1,
          dzbid:-1}
      },
      dzb:{
        name:'zhibuclassify',
        value:'??',
        show:false,
        params:{
          dwid:-1,
          dzbid:-1}
      }
    },
  },
  actions:{// 然后给 actions 注册事件处理函数，当这个函数被触发时，将状态提交到 mutaions中处理
    Login(state,data){
      return new Promise(function (resolve, reject) {
        Common.AjaxPost(data,true)
        .then(function(e){
          Common.WriteStorage('user',JSON.stringify(e));
          Common.WriteStorage('token',e.token);
          state.state.user=e;
          resolve(e);
        },function(e){
          resolve(false);
        });
      });
    },
    GetZbById(state,data){
      return new Promise(function (resolve, reject) {
        let postdata={
          type:"GetZbById",
          split:JSON.stringify(state.state.Dw_DzbID)
        }
        if(typeof data==="object"){
          if(data.dwid && data.dzbid){
            postdata.split=JSON.stringify(data)
          }
        }
        Common.AjaxPost(postdata,true)
        .then(function(e){
          resolve(e)
        },function(){
          resolve(false)
        })
        .catch(function(reason) {
          resolve(false)
            console.log('catch:', reason);
        });
      });
    },
    GetZbByIdstate(state,data){
      let finds=state.state.dwdzbs.filter(function(value){
        return value.dwid==data.dwid && value.dzbid==data.dzbid
      })
      if(finds.length==1){
        return finds[0];
      }
      else{
        if(data.dwid==-2||data.dzbid==-2)
        {
          return false;
        }
        let postdata={
          type:"GetZbById",
          split:JSON.stringify(data)
        }
        let e=Common.AjaxPostSync(postdata,true).Data;
        if(typeof e==="object"){
          if(Array.isArray(e.childs)){
            state.state.dwdzbs.push(e.PostThis);
            for (let index = 0; index < e.childs.length; index++) {
              const element = e.childs[index];
              let finds2=state.state.dwdzbs.filter(function(value){
                return (value.dwid==element.dwid && value.dzbid==element.dzbid)
              })
              if(finds2.length==0){
                state.state.dwdzbs.push(element);
              }
    
            }
            return e.PostThis;
          }
          return false;
        }
        else{
          return false;
        }
      }
    },
    Getzbdetails(state,data){ //为了保证实时更新 需要每次都要请求
      return new Promise(function (resolve, reject) {
        let postdata={
          type:"getzbdetails",
        }
        if(typeof data==="object"){
          postdata.split=JSON.stringify(data)
        }
        else{
          postdata.split=JSON.stringify(state.state.Dw_DzbID)
        }
        Common.AjaxPost(postdata,true)
        .then(function(e){
          resolve(e.ZhuzhiDetails)
        },function(){
          resolve(false)
        })
        .catch(function(reason) {
          resolve(false)
            console.log('catch:', reason);
        });
      });
    },
    GetPersons(state){ //为了保证实时更新 需要每次都要请求
      return new Promise(function (resolve, reject) {
        let postdata={
          type:"GetPersons",
          split:JSON.stringify(state.state.Dw_DzbID)
        }
        Common.AjaxPost(postdata,true)
        .then(function(e){
          let Persons=[];
          for (let index = 0; index < e.Persons.length; index++) {
            const element = e.Persons[index];
            let dataT=new Date().getTime();
            element.img=element.img+"?"+dataT;
            Persons.push(element);
          }
          resolve(Persons) //如果更新图片没有对图片信息进行时间节点保证的话 就将出现不刷新的情况
        },function(){
          resolve(false)
        })
        .catch(function(reason) {
          resolve(false)
            console.log('catch:', reason);
        });
      });
    },
    GetUser(state){
      try{
        state.state.user=JSON.parse(Common.GetStorage('user'));
        store.dispatch('GetActiveDwId_DzbId')
        store.dispatch('GetActivebreadcrumb')
        //检查token
        return true;
      }catch(er){
        return false;
      }
    },
    GetTabs(state,data)
    {
      let postdata={
        type:"gettab",
        split:JSON.stringify({
          dwid:state.state.Dw_DzbID.dwid,
          dzbid:state.state.Dw_DzbID.dzbid,
          name:data,
        })                 
      }
      return Common.AjaxPost(postdata,true);
    },
    GetGns(state,data)
    {
      let postdata={
        type:"gettypea",
        split:JSON.stringify({
          dwid:state.state.Dw_DzbID.dwid,
          dzbid:state.state.Dw_DzbID.dzbid,
          name:data,
        })                 
      }
      return Common.AjaxPost(postdata,true);
    },
    Getallgns(state,data)
    {
      let postdata={
        type:"get_allgns",
        split:JSON.stringify({
          typea:data,
        })                 
      }
      return Common.AjaxPost(postdata,true);
    },
    GetActiveDwId_DzbId(state){
      try{
        state.state.Dw_DzbID=
        {
          dwid:Common.GetStorage('avtivedwid')-0,
          dzbid:Common.GetStorage('avtivedzbid')-0,
        }
      }catch(er){
        return false;
      }
    },
    WriteActiveDwId_DzbId(state,data){
      try{
        Common.WriteStorage('avtivedwid',data.dwid);
        Common.WriteStorage('avtivedzbid',data.dzbid);
        state.state.Dw_DzbID=data;
      }catch(er){
        return false;
      }
    },
    WriteActivebreadcrumb(state,data){
      try{
        let indexA=-1;
        for (let index = 0; index < state.state.breadcrumb.length; index++) {
            const element = state.state.breadcrumb[index];
            if(element.name==data.name){
                indexA=index;
                break;
            }
        }
        if(indexA==-1){
          state.state.breadcrumb.push(data);
        }
        Common.RemoveStorage('breadcrumb');
        Common.WriteStorage('breadcrumb',JSON.stringify(state.state.breadcrumb));
      }catch(er){}
    },
    GetActivebreadcrumb(state){
      try{
        state.state.breadcrumb=JSON.parse(Common.GetStorage('breadcrumb'));
      }catch(er){}
    },
    deletebreadcrumb(state,index){
      try{
        state.state.breadcrumb.splice(index,state.state.breadcrumb.length-index);
        Common.RemoveStorage('breadcrumb');
        Common.WriteStorage('breadcrumb',JSON.stringify(state.state.breadcrumb));
      }catch(er){}
    },
    GetDangfeis(state,data){ //为了保证实时更新 需要每次都要请求
      return new Promise(function (resolve, reject) {
        let element=data.element;
        let postdata={
          type:"serchdangfei",
        }
        let pd={};
        if(data.year){pd.year=data.year;}
        if(data.month){pd.month=data.month;}

        if(data.dwid){
          pd.dwid=data.dwid;
        }
        else{
          pd.dwid=state.state.Dw_DzbID.dwid
        }
        if(data.dzbid){
          pd.dzbid=data.dzbid;
        }
        else{
          pd.dzbid=state.state.Dw_DzbID.dzbid
        }
        postdata.split=JSON.stringify(pd)
        Common.AjaxPost(postdata,true)
        .then(function(e){
          resolve({
            element:element,
            dfeis:e.dfeis
          });
        },function(){
          reject(false)
        })
        .catch(function(reason) {
          reject(false);console.log('catch:', reason);
        });
      });
    },
    GetBAYRJs(state,data){ //为了保证实时更新 需要每次都要请求
      return new Promise(function (resolve, reject) {
        let element=data.element;
        let postdata={
          type:"serchbayrj",
        }
        let pd={};
        if(data.year){pd.year=data.year;}
        if(data.month){pd.month=data.month;}

        if(data.dwid){
          pd.dwid=data.dwid;
        }
        else{
          pd.dwid=state.state.Dw_DzbID.dwid
        }
        if(data.dzbid){
          pd.dzbid=data.dzbid;
        }
        else{
          pd.dzbid=state.state.Dw_DzbID.dzbid
        }
        postdata.split=JSON.stringify(pd)
        Common.AjaxPost(postdata,true)
        .then(function(e){
          resolve({
            element:element,
            dfeis:e.bayrjs
          });
        },function(){
          reject(false)
        })
        .catch(function(reason) {
          reject(false);console.log('catch:', reason);
        });
      });
    },
  },
  // 更新状态或方法
   mutations : {
  },
  // 获取状态信息
   getters : {
  }




});
export default store;