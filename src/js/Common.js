import $ from 'jquery'
var Common = {};
Common.Config = {
    url:'http://localhost:8000/getdata?',
    // url: window.Glod.apiurl + 'getdata?',
    this: null,
  },
  Common.WriteStorage = function (name, value) {
    let storage = window.localStorage;
    storage[name] = value;
  }

Common.GetStorage = function (name) {
  let storage = window.localStorage;
  return storage[name];
}
Common.ClearStorage = function () {
  let storage = window.localStorage;
  storage.clear();
}
Common.RemoveStorage = function (name) {
  let storage = window.localStorage;
  storage.removeItem(name);
}
Common.musicObject = {
  music: null,
  playing: false,
  initplayend: false,
  playmusic: function (src) {
    if (Common.musicObject.initplayend == false) {
      Common.musicObject.music = new Audio();
      Common.musicObject.music.addEventListener("ended", function () {
        Common.musicObject.playing = false;
      });
      Common.musicObject.music.preload = "auto"
      Common.musicObject.initplayend = true;
    }

    if (Common.musicObject.music.src == src) {
      if (Common.musicObject.playing == true) {
        Common.musicObject.music.pause();
        Common.musicObject.playing = false;
      } else {
        Common.musicObject.playing = true;
        Common.musicObject.music.play();
      }
    } else {
      if (Common.musicObject.music.src == "") {
        Common.musicObject.music.src = src;
        Common.musicObject.music.play();
        Common.musicObject.playing = true;
      } else {
        Common.musicObject.music.src = src;
        if (Common.musicObject.playing == true) {
          Common.musicObject.music.play();
        }
      }


    }

  },
  closemusic() {
    if (Common.musicObject.playing == true) {
      Common.musicObject.music.pause();
    }
  }
}
Common.AjaxPost = function (JsonData, withtoken = false, type = "get") {
  return new Promise(function (resolve, reject) {
    try {
      if (withtoken === true) {
        JsonData.token = Common.GetStorage('token');
      }
      let ajaxSet = {
        url: Common.Config.url,
        type: type,
        data: JsonData,
        dataType: "json",
        xhrFields: {
          withCredentials: true
        },
        crossDomain: true,
      }
      let ajaxSeted = $.ajax(ajaxSet);
      ajaxSeted.then(function (e) {
        if (e.Status == true) {
          resolve(e.Data);
        } else {
          if (e.LoginEd == false) {
            //跳出登陆界面
            Common.Tologin();
          }
          // Common.ShowMessage(e.Message);
          reject(false);
        }
      }, function (e) {
        Common.ShowMessage('网络连接或者配置错误');
        reject(false);
      });
    } catch (e) {
      Common.ShowMessage('网络连接或者配置错误');
      reject(false);
    }
  });
}
Common.AjaxPostSync = function (JsonData, withtoken = false, type = "get") {
  let back = {};
  if (withtoken === true) {
    JsonData.token = Common.GetStorage('token');
  }
  $.ajax({
    url: Common.Config.url,
    type: type,
    async: false,
    dataType: "json",
    data: JsonData,
    xhrFields: {
      withCredentials: true
    },
    crossDomain: true,
    success: function (e) {
      back = e;
    },
    error: function () {
      back.Status = false;
      back.Data = '网络连接或者配置错误';
    }
  });
  return back;
}
Common.WebSocket = {
  ws: null,
  InitWebSocket: function () {
    let urlWs = Common.Config.url.replace('http', 'ws').replace('https', 'ws');
    Common.WebSocket.ws = new WebSocket(urlWs);
    Common.WebSocket.ws.onopen = function (e) {
      console.log('onopen', e);
    };
    Common.WebSocket.ws.onerror = function (e) {
      console.log('error', e);
    }
    Common.WebSocket.ws.onclose = function (e) {
      console.log('close', e);
    }
    Common.WebSocket.ws.onmessage = Common.WebSocketmessage;

  }
}
// Common.WebSocket.InitWebSocket();
Common.WebSocketsend = function (message) {
  if (Common.WebSocket.ws) {

  } else {

  }
  if (Common.WebSocket.ws) {

  }
  if (typeof message === "string") {
    Common.WebSocket.ws.binaryType = 'nodebuffer'
  } else if (typeof message === "blob") {
    Common.WebSocket.ws.binaryType = 'blob'
  } else if (typeof message === "arraybuffer") {
    Common.WebSocket.ws.binaryType = 'arraybuffer'
  }
  Common.WebSocket.ws.send(message);
}
Common.WebSocketmessage = function (data) {
  console.log(data);
}
Common.ShowMessage = function (message) {
  if (Common.Config.this) {
    Common.Config.this.$message({
      message: message,
      type: 'error',
      duration: 1000
    })
    return;
  }
}
Common.Tologin = function () {
  if (Common.Config.this) {
    let thisvue = Common.Config.this;
    let find = false;
    do {
      thisvue = thisvue.$parent;
      if (thisvue.showLoginagain !== undefined) {
        find = true;
      } else {
        find = false;
      }
    } while (!find)
    console.log(thisvue);
    thisvue.showLoginagain = true;
    return;
  }
}
Common.ValidPage = function () {
  var width = screen.width;
  var height = screen.height;
  if (width != 1920) {
    return "经系统检测，你的屏幕分辨率为 " + width + "*" + height + ",为了更好的体验，请使用1920*1080分辨率";
  }
  return ''
}
Common.setCookie = function (c_name, value, expire) {
  var date = new Date()
  date.setSeconds(date.getSeconds() + expire)
  document.cookie = c_name + "=" + escape(value) + "; expires=" + date.toGMTString() + "; path=/"
}
Common.getCookie = function (c_name) {
  if (document.cookie.length > 0) {
    let c_start = document.cookie.indexOf(c_name + "=")
    // console.log(c_start)
    if (c_start != -1) {
      c_start = c_start + c_name.length + 1
      let c_end = document.cookie.indexOf(";", c_start)
      if (c_end == -1) c_end = document.cookie.length
      return unescape(document.cookie.substring(c_start, c_end))
    }
  }
  return ""
}
Common.checkCookie = function (c_name) {
  if (Commons.getCookie(c_name) == '') {
    return false
  } else {
    return true
  }
}
Common.isNumber = function (val) {
  var regPos = /^\d+(\.\d+)?$/; //非负浮点数
  var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/;
  if (regPos.test(val) || regNeg.test(val)) {
    return true;
  } else {
    return false;
  }
}
export default Common