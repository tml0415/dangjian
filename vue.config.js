module.exports = {
	outputDir: 'dist', // 输出文件目录
	assetsDir: 'assets',
	indexPath: 'index.html',
	pages: undefined,
	lintOnSave: true, // eslint-loader 是否在保存的时候检查
	runtimeCompiler: false, //是否使用包含运行时编译器的 Vue 构建版本。设置为 true 后你就可以在 Vue 组件中使用 template 选项了，但是这会让你的应用额外增加 10kb 左右
	transpileDependencies: [], //默认情况下 babel-loader 会忽略所有 node_modules 中的文件。如果你想要通过 Babel 显式转译一个依赖，可以在这个选项中列出来
	filenameHashing: true,
	productionSourceMap: false, // 生产环境是否生成 sourceMap 文件
	crossorigin: undefined, //
	integrity: false,
	configureWebpack: {},
	chainWebpack: (config) => {}, // 是一个函数，会接收一个基于 webpack-chain 的 ChainableConfig 实例。允许对内部的 webpack 配置进行更细粒度的修改。
	css: { // css相关配置
		extract: true, // 是否使用css分离插件 ExtractTextPlugin
		sourceMap: false, // 开启 CSS source maps?
		loaderOptions: { // css预设器配置项
			css: {
				// 这里的选项会传递给 css-loader
			},
			postcss: {},
			sass: {},
			less: {},
		},
		modules: false // 启用 CSS modules for all css / pre-processor files.
	},
	parallel: require('os').cpus().length > 1, // enabled by default if the machine has more than 1 cores
	// dll: false, // 是否启用dll
	pwa: {}, // PWA 插件相关配置 https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa

	pluginOptions: { // 第三方插件配置

	},

}